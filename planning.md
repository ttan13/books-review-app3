# Planning

[X] Make a virtual environment
    <!-- python -m venv .venv -->
[X] Activate virtual environment
    <!-- source ./.venv/bin/activate -->
[X] Install django
    <!-- pip install django -->
[X] Create a django project
    <!-- django-admin startproject <project name> .  -->
    REMEMBER THE DOT!
[X] Add a django app to the project
    <!-- python manage.py startapp <app name> -->
[X] Make migrations
    <!-- python manage.py makemigrations  -->
[X] Migrate
    <!-- python manage.py migrate -->
[X] Start django server
    <!-- python manage.py runserver -->
[X] Configure the project to use the app
    <!--settings.py --> INSTALLED_APPS -->
[X] Create urls for app
    <!-- urls.py -->
[X] In the apps urls.py, prepare it for    configuration
    <!-- from django.urls import path
    urlpatterns = [] -->
[X] Update project urls.py file 
    <!-- path("reviews/", include("reviews.urls")) -->
[X] Update the database for Django
    <!-- python manage.py migrate -->
[X] Run the server
    <!-- python manage.py runserver -->
[X] Create a model
[X] Update the database by making migrations
    <!-- python manage.py makemigrations -->
    <!-- python manage.py migrate -->
[X] Run the server
    <!-- python manage.py runserver -->
[X] Create a 'templates' directory
[X] Create a 'reviews' directory
[X] Create a 'main' template
[X] Create a view function: gathers data and sends the html back to the browser
[X] Create a url mapping for the newly created view
[X] Run server to check that it works
[X] Create a 'static' directory
[X] Create a css file
[X] Link the css file in the main.html file